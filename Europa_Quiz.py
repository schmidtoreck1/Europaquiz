from random import *
RichtigeAntwortanzahl = 0
frageNummer = 0
i = 0

Frage = ["Wie viele Säulen hat das Brandenburger Tor?", "Wie hoch ist der höchste Berg in Frankreich in Metern?",
        "Größtes europäisches Land?", "Wie viele Millionen Einwohner hatte Italien 2021?",
         "Wie viel Millionen Liter befinden sich ca. in den europäischen Meeren?", "Wie alt ist Olaf Scholz?",
         "Wie alt ist Angela Merkel?", "Wie viele Millionen Jahre  ist  Europa alt?",
         "Wie heißt der holländische Präsident?"]
RichtigeAntwort = ["12", "4810", "Frankreich", "59", "59", "65", "69", "250", "Mark Rutte"]
Anz_fragen = len(Frage)
die_frage_hatten_wir_schon = [0] * Anz_fragen
print("Herzlich willkommen bei unserem Europaquiz. Lassen Sie uns starten:")

while i < Anz_fragen-1:
    frageNummer = randrange(1, Anz_fragen, 1)
    if die_frage_hatten_wir_schon[frageNummer] == 0:
        print(Frage[frageNummer])
        antwort = str(input("Antwort? "))
        if antwort == RichtigeAntwort[frageNummer]:
            RichtigeAntwortanzahl += 1
            print("richtig")

        else:
            print("falsch")
            print("Richtige Antwort: ",RichtigeAntwort[frageNummer])
        i+=1
        die_frage_hatten_wir_schon[frageNummer] = 1
print("Du hast den Score von:",RichtigeAntwortanzahl)
antwort = str(input("Vielen Dank, dass Sie an unserem Europaquiz teilgenommen haben. Beenden Sie das Programm mit F für Finish  "))